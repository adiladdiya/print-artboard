# Print Artboard

generate new Artboard or resize existing ones to common documents sizes (A3, A4, A5, legal, letter ...) or custom values with unite you use (mm, in ...).

control the resolution of the document.

## Getting Started

Use the plugin menu or shortcut (the same for mac os or windows):
```
- Generate or resize : 'Ctrl+F'
- Settings : 'Ctrl+Shift+F'
```

## Settings

define your artboard preferences using the settings form:

![alt text](screenshot.png)

### Prerequisites

Adobe XD 13.0 or higher
